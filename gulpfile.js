const gulp = require('gulp');
const sass = require('gulp-sass');
const sassGlob = require("gulp-sass-glob");
const pug = require('gulp-pug'); 
const browserSync = require('browser-sync').create();

// compile scss into css
function style() {
  //  1. where is my scss file
  return gulp.src('src/scss/**/*.scss')
  //  2. pass that file through sass compiler
    .pipe(sass().on('error', sass.logError))
  //  3. where do I save the compiled CSS?
    .pipe(gulp.dest('src/css'))
    .pipe(sassGlob()) // Accepts file globbing
  // 4. stream changes to all browser
    .pipe(browserSync.stream());
}

function pugSrc() {
  return gulp.src('src/pug/**/*.pug')
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest('src'))
  .pipe(browserSync.stream())
}

function watch () {
  browserSync.init({
    server: {
      baseDir: 'src'
    }
  });
  gulp.watch('src/scss/**/*.scss', style);
  gulp.watch('src/pug/**/*.pug', pugSrc);
  gulp.watch('src/js/**/*.js').on('change', browserSync.reload);
}

exports.style = style;
exports.pugSrc = pug;
exports.watch = watch;
